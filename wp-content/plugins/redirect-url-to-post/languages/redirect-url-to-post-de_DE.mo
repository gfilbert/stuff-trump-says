��          �      l      �     �  5   �  .   $  0   S  b   �  !   �  q   	  $   {  	   �     �     �  R   �          &  2   ;  .   n  b   �  �         �  -   �  ^       l  =   y  3   �  5   �  u   !  /   �  �   �  ,   U	     �	     �	     �	  S   �	     
     
  C   /
  5   s
  ]   �
           -   $                      	                                         
                                   Chatty Mango Error: The parameter 'custom' requires also 'orderby' Error: Unrecognized value of parameter 'order' Error: Unrecognized value of parameter 'orderby' Go to a random post from the past month: <a %s>%s</a> (or to the front page, if nothing was found) Go to a random post: <a %s>%s</a> Go to your latest post that was created at least 10 minutes ago: <a %s>%s</a> (We replaced all spaces by "%%20".) Go to your latest post: <a %s>%s</a> Have fun! Help Here are some examples: Please find a description of all parameters in the <a %s>plugin documentation</a>. Random Post Redirect URL to Post Redirects to a post based on parameters in the URL Thank you for installing Redirect URL to Post! This plugin doesn't have any settings. You configure it entirely through the URL query parameters. You can use these links anywhere in your posts and pages, in menus or in buttons. You can even send them in a newsletter. Or you could try the shortcode [redirect_to_post_button] in a page or post. https://chattymango.com/ https://chattymango.com/redirect-url-to-post/ Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-01-26 16:41+0000
PO-Revision-Date: 2018-01-26 18:06+0100
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Last-Translator: Christoph Amthor
Language: de_DE
 Chatty Mango Fehler: Der Parameter ‘custom' benötigt auch ‘orderby’ Fehler: Unbekannter Wert des Parameters ‘order’ Fehler: Unbekannter Wert des Parameters ‘orderby’ Gehe zu einem zufälligen Beitrag des letzten Monats: <a %s>%s</a> (oder zur Startseite, falls nichts gefunden wurde) Gehe zu einem zufälligen Beitrag: <a %s>%s</a> Gehe zu deinem letzten Beitrag, der vor mindestens 10 Minuten erstellt wurde: <a %s>%s</a> (Wir haben alle Leerzeichen durch "%%20" ersetzt.) Gehe zu deinem letzten Beitrag: <a %s>%s</a> Viel Spaß! Hilfe Hier sind ein paar Beispiele: Eine Beschreibung aller Parameter findest du in der <a %s>Plugin-Dokumentation</a>. Zufälliger Beitrag Redirect URL to Post Leitet zu einem Beitrag weiter, in Abhängigkeit von URL-Parametern Danke für das Installieren von Redirect URL to Post! Dieses Plugin hat keine Einstellungen. Du konfigurierst es völlig durch URL Query-Parameter. Du kannst diese Links irgendwo in deinen Beiträgen und Seiten, in Menüs oder in Schaltern verwenden. Du kannst sie sogar in einem Newsletter versenden. Oder du könntest den Shortcode [redirect_to_post_button] in einer Seite oder einem Beitrag ausprobieren. https://chattymango.com/ https://chattymango.com/redirect-url-to-post/ 