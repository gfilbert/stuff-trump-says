��          �      l      �     �  5   �  .   $  0   S  b   �  !   �  q   	  $   {  	   �     �     �  R   �          &  2   ;  .   n  b   �  �         �  -   �  N       \  5   i  .   �  0   �  b   �  !   b  q   �  $   �  	   	     %	     *	  R   B	     �	     �	  2   �	  .   �	  b   
  �   {
     A  -   Z                      	                                         
                                   Chatty Mango Error: The parameter 'custom' requires also 'orderby' Error: Unrecognized value of parameter 'order' Error: Unrecognized value of parameter 'orderby' Go to a random post from the past month: <a %s>%s</a> (or to the front page, if nothing was found) Go to a random post: <a %s>%s</a> Go to your latest post that was created at least 10 minutes ago: <a %s>%s</a> (We replaced all spaces by "%%20".) Go to your latest post: <a %s>%s</a> Have fun! Help Here are some examples: Please find a description of all parameters in the <a %s>plugin documentation</a>. Random Post Redirect URL to Post Redirects to a post based on parameters in the URL Thank you for installing Redirect URL to Post! This plugin doesn't have any settings. You configure it entirely through the URL query parameters. You can use these links anywhere in your posts and pages, in menus or in buttons. You can even send them in a newsletter. Or you could try the shortcode [redirect_to_post_button] in a page or post. https://chattymango.com/ https://chattymango.com/redirect-url-to-post/ Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-01-26 16:41+0000
PO-Revision-Date: 2018-01-26 17:44+0100
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Last-Translator: 
Language: en_US
 Chatty Mango Error: The parameter 'custom' requires also 'orderby' Error: Unrecognized value of parameter 'order' Error: Unrecognized value of parameter 'orderby' Go to a random post from the past month: <a %s>%s</a> (or to the front page, if nothing was found) Go to a random post: <a %s>%s</a> Go to your latest post that was created at least 10 minutes ago: <a %s>%s</a> (We replaced all spaces by "%%20".) Go to your latest post: <a %s>%s</a> Have fun! Help Here are some examples: Please find a description of all parameters in the <a %s>plugin documentation</a>. Random Post Redirect URL to Post Redirects to a post based on parameters in the URL Thank you for installing Redirect URL to Post! This plugin doesn't have any settings. You configure it entirely through the URL query parameters. You can use these links anywhere in your posts and pages, in menus or in buttons. You can even send them in a newsletter. Or you could try the shortcode [redirect_to_post_button] in a page or post. https://chattymango.com/ https://chattymango.com/redirect-url-to-post/ 