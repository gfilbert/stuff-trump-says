=== Redirect URL to Post ===
Contributors: camthor
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FD5ZU4EEBGSC8
Tags: redirect, random, recent, random post, filter
Requires at least: 4.0
Tested up to: 5.2.2
Stable tag: 0.12.0
Requires PHP: 5.4
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Automatically redirect to your latest, oldest, random, or other post through a custom URL

== Description ==

This plugin provides an URL that takes you directly to a post in *single-post view*. This post is determined by the query parameter **?redirect_to=** and optional others. You can, for example, redirect to your latest, oldest or a random post, or to your second latest, or the oldest post that is not older than two months.

Simply enter the URL of your WordPress site into your browser and add **?redirect_to=...** to the end. You can, of course, also use that URL in any link, in a menu, a button and so on. The plugin will recognize it and redirect the browser to the right post or page.

Possible values for **redirect_to** are:

* **last** or **latest** – The URL will redirect to the last (latest) post.
* **first** or **oldest** – The URL will redirect to the first (oldest) post.
* **random** – The URL will redirect to a random post.
* **custom** – The post will be determined according to the mandatory parameter orderby and the optional parameter order.

You can also limit the scope of considered posts by additional filter parameters, such as **&s=searchaword** or **&cat=2**.

= Caching =

The plugin offers [caching of database results](https://documentation.chattymango.com/documentation/redirect-url-to-post/getting-started-redirect-url-to-post/other-parameters/?pk_campaign=rutp&pk_kwd=readme#Caching "plugin website").

= Settings and Parameters =

There is no settings page in the backend. You configure the plugin entirely through the query parameters in the URL.

Please find more information about parameters and troubleshooting [on the plugin website](https://documentation.chattymango.com/documentation/redirect-url-to-post/?pk_campaign=rutp&pk_kwd=readme "plugin website").


= Examples for URLs =

Note: Replace "http://www.example.com/" with your own website location. Spaces are written as "%20".

http://www.example.com/?redirect_to=latest - **redirects to the latest post**

http://www.example.com/?redirect_to=random&pk_campaign=random - **redirects to a random post and tracks the visit**

http://www.example.com/?redirect_to=random&count=10 - **redirects to a random post among the 10 latest posts**

http://www.example.com/?redirect_to=random&count=10&bias=80 - **redirects to a random post. The plugin picks one from the latest 10 with a probability of 80% and from the rest with a probability of 20%**

http://www.example.com/?redirect_to=random&count=10&offset=1 - **redirects to a random post among the 10 posts that come after the latest**

http://www.example.com/?redirect_to=random&after=1%20month%20ago - **redirects to a random post among the posts that are not older than 1 month**

http://www.example.com/?redirect_to=latest&exclude=4,7 - **redirects to the latest post, excluding the posts with the IDs 4 and 7**

http://www.example.com/?redirect_to=latest&offset=1 - **redirects to the second latest post**

http://www.example.com/?redirect_to=custom&orderby=comment_count&order=DESC - **redirects to the post with the most comments**

http://www.example.com/?redirect_to=latest&s=iaido&default_redirect_to=12&cache=200 - **redirects to the latest post that contains the word 'iaido' or, if nothing can be found, to the page or post with the ID 12; use a cache with a 200 second lifetime**


= Button =

The plugin also provides a shortcode [redirect_to_post_button] to create a simple button. Some [parameters](https://documentation.chattymango.com/documentation/redirect-url-to-post/getting-started-redirect-url-to-post/redirect-button/?pk_campaign=rutp&pk_kwd=readme#Shortcode_to_create_button "shortcodes") are available.

A button that links to a random post is a great way to increase your visitors' on-site engagement and therefore your **SEO ranking**!


**If you find this plugin useful, please give it a [5-star rating](https://wordpress.org/support/plugin/redirect-url-to-post/reviews/?filter=5 "reviews"). Thank you!**

Follow us on [Facebook](https://www.facebook.com/chattymango/) or [Twitter](https://twitter.com/ChattyMango).

= Check out my other plugins =

* [Tag Groups](https://wordpress.org/plugins/tag-groups/)

== Installation ==

1. Find the plugin in the list at the admin backend and click to install it. Or, upload the ZIP file through the admin backend. Or, upload the unzipped redirect-url-to-post folder to the /wp-content/plugins/ directory.
2. Activate the plugin through the ‘Plugins’ menu in WordPress.

[plugin website](https://chattymango.com/redirect-url-to-post/?pk_campaign=rutp&pk_kwd=readme "plugin website")

After the first activation you will find a screen with some examples of URLs for your blog.

== Frequently Asked Questions ==

= 1. What if more than one post match the criteria (e.g. two have the same comment_count)? =

There can be only *one* winner. The post that would be first in the list (as determined by WP) beats all others.

= 2. Since WordPress version 4.8, the shortcodes in the widgets stopped working correctly =

That is a problem caused by the new editor in WordPress widgets. You can avoid it by using a text-only widget, like [Classic Text Widget](https://wordpress.org/plugins/classic-text-widget/ "plugin")

= 3. The random parameter redirects always to the same post =

You probably use a caching plugin or service that also caches query strings. Try adding an exception for the string "redirect_to=". If you use Cloudflare, you can try their [Page Rules]( https://chattymango.freshdesk.com/solution/articles/5000750256-the-redirect-to-random-parameter-always-redirects-to-the-same-post "Cloudflare").

= 4. Which URL can serve as the base? =

Obviously only URLs of the WordPress blog where this plugin is installed.

= 5. The post cannot be found but I'm sure that it's there and that it's public =

The most common reason is that this post belongs to a special post type. Try the parameter "post_type", for example "post_type=product".

= 6. Can I help translate? =

Thank you! Please [continue here](https://translate.wordpress.org/projects/wp-plugins/redirect-url-to-post).

== Screenshots ==

== Changelog ==

= 0.12.0 =

FEATURES

* New parameters comment_count, comment_count_min (>=) and comment_count_max (<=) to filter by comment count. The value is always an integer. comment_count also takes negative values, which means "not". Requires WordPress 4.9+.

OTHER

* Whitelisted more tracking parameters for Google and Matomo Analytics.

= 0.11.2 =

BUG FIXES

* Fixed values of parameter "cat" getting lost.
* Fixed incorrect inclusion of posts outside of "count" when using "bias".

= 0.11.1 =

OTHER

* Button parameter 'params' now formatted as: “key1=value1|key2=value2| …” (Old format will continue working.)

= 0.11.0 =

FEATURES

* New parameter 'bias', to be used together with '?redirect_to=random' and 'count'. The value of this parameter is a percentage that determines how much the posts determined by 'count' will be preferred over the rest. Example: www.example.com/?redirect_to=random&count=10&bias=80 -> The most recent 10 posts will be preferred with an 80% probability. Can be used together with 'offset'.

OTHER

* Support for PHP versions older than 5.6 will be dropped in near future.

= 0.10.0 =

FEATURES

* New parameter "count" that can be used together with "random" to limit the pool of posts to the most recent (count has positive number) or first  (count has negative number). Can be used together with "offset".

OTHER

* Changed default for cache to 60 seconds (Override it with cache=0)
* Random posts are now always determined by PHP, not the database

= 0.9.0 =

FEATURES

* New parameter "offset" to retrieve the 2nd, 3rd, (integer > 3)th post. Default is 0 (i.e. 1st). For example "offset=1" means 2nd post. An offset too high will trigger the procedure for the case when no post was found.
* New parameter "suppress_filters" . Default is 1 (on). Turn off ("suppress_filters=0") when you use WPML so that WPML can filter by language.

BUG FIXES

* fixed problem when latest post was not published


= Older Versions =

The complete changelog is available [here](https://chattymango.com/redirect-url-to-post/redirect-url-post-changelog/?pk_campaign=rutp&pk_kwd=readme).

== Upgrade Notice ==
