<?php
/*
Plugin Name: Redirect URL to Post
Plugin URI: https://chattymango.com/redirect-url-to-post/
Description: Redirects to a post based on parameters in the URL
Author: Chatty Mango
Author URI: https://chattymango.com/
Version: 0.12.0
License: GNU GENERAL PUBLIC LICENSE, Version 3
Text Domain: redirect-url-to-post
Domain Path: /languages
*/

// Don't call this file directly
if ( ! defined( 'ABSPATH') ) {
  die;
}

class RedirectUrlToPost
{

  /**
  * 	Initial setup: Register the filter and action
  *
  */
  public function __construct() {

    add_action( 'admin_notices', array( $this, 'admin_notice' ) );

    add_action( 'send_headers', array( $this, 'redirect_post' ) );

    // redirect_to_random_button deprecated since versions after 0.5.2, only kept for backward compatibility
    add_shortcode( 'redirect_to_random_button', array( $this, 'redirect_to_post_button' ) );

    add_shortcode( 'redirect_to_post_button', array( $this, 'redirect_to_post_button' ) );

    add_filter( 'widget_text', 'do_shortcode' );

    if ( is_admin() ) {

      add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'add_help_link' ), 10 );

    }

  }


  /**
  * 	Does the actual work
  *
  */
  public function redirect_post() {

    global $redirect_post_query_run;

    // Prevent being triggered again when executing the query
    if ( $redirect_post_query_run == 0 ) {

      $redirect_post_query_run++;

      // Retrieve search criteria from GET query
      // Use sanitized $_GET to be independent from current state of WP Query and possible unavailability of GET parameters.
      if ( ! empty( $_GET['redirect_to'] ) ) {

        // Can use sanitize_key because only small letters and underscores needed
        $redirect_to = sanitize_key( $_GET['redirect_to'] );

        // Set default args
        $args_filter_or_custom = array(
          'fields'              => 'ids',
          'order'               => 'DESC',
          'post_type'           => 'post',
          'suppress_filters'    => true,
          'ignore_sticky_posts' => true,
          'posts_per_page'      => 1,
          'post_status'         => 'publish'
        );


        /**
        *  Post filters
        *
        *  Get standard parameters that make sense to use
        */
        $args_default_values = array(
          'cat', // strangely 'category' not working
          'category_name',
          'date_query_after', // alias for after
          'after', // translates to date_query construct
          'date_query_before', // alias for before
          'before', // translates to date_query construct
          'day',
          'hour',
          'ignore_sticky_posts',
          'minute',
          'monthnum',
          'offset',
          'order',
          'orderby',
          'post_type', // requires has_archive for that post_type
          's',
          'second',
          'suppress_filters',
          'tag',
          'w',
          'author',
          'author_name',
          'has_password',
          'tag_id',
          'exclude'
        );

        foreach ( $args_default_values as $value ) {

          if ( isset( $_GET[ $value ] ) &&  '' !== $_GET[ $value ] ) {

            if ( 'exclude' == $value ) {

              $args_filter_or_custom['post__not_in'] = array_map( 'intval', array_map( 'trim', explode( ',', $_GET['exclude'] ) ) );

            } elseif ( 'offset' == $value ) {

              $args_filter_or_custom['offset'] = abs( intval( $_GET['offset'] ) );

            } elseif ( 'suppress_filters' == $value ) {

              $args_filter_or_custom['suppress_filters'] = $_GET['suppress_filters'] ? true : false;

            } else {

              // Sanitized with sanitize_text_field because some values may be uppercase or spaces
              $args_filter_or_custom[ $value ] = sanitize_text_field( $_GET[ $value ] );

            }

          }

        }

        /**
        * Special processing for 'date_query_before' and 'date_query_after'
        */
        $date_query_items = array();

        if ( ! empty( $args_filter_or_custom['date_query_before'] ) ) {

          $date_query_items['before'] = $args_filter_or_custom['date_query_before'];

          unset( $args_filter_or_custom['date_query_before'] );

        }

        if ( ! empty( $args_filter_or_custom['before'] ) ) {

          $date_query_items['before'] = $args_filter_or_custom['before'];

          unset( $args_filter_or_custom['_before'] );

        }

        if ( ! empty( $args_filter_or_custom['date_query_after'] ) ) {

          $date_query_items['after'] = $args_filter_or_custom['date_query_after'];

          unset( $args_filter_or_custom['date_query_after'] );

        }

        if ( ! empty( $args_filter_or_custom['after'] ) ) {

          $date_query_items['after'] = $args_filter_or_custom['after'];

          unset( $args_filter_or_custom['after'] );

        }

        /**
        * Construct the 'date_query' array
        */
        if ( ! empty( $date_query_items ) ) {

          $args_filter_or_custom['date_query'] = array(
            $date_query_items
          );

        }


        if ( ! empty( $_GET['count'] ) ) {

          $count = intval( $_GET['count'] );

        } else {

          $count = 0;

        }

        if ( ! empty( $_GET['bias'] ) ) {

          $random_bias = intval( $_GET['bias'] );

        } else {

          $random_bias = false;

        }


        // caching
        // We can override the settings with a constant so that visitors cannot maliciously overuse the database.
        if ( defined( 'CHATTY_MANGO_RUTP_CACHE' ) ) {

          $caching_time = intval( CHATTY_MANGO_RUTP_CACHE );

        } else {

          if ( isset( $_GET['cache'] ) ) {

            $caching_time = intval( $_GET['cache'] );

          } else {

            $caching_time = 60;

          }

        }

        if ( isset( $_GET['comment_count'] ) ) {

          $compare = ( substr( $_GET['comment_count'], 0, 1 ) == '-' ) ? '!=' : '=';

          $args_filter_or_custom['comment_count'] = array(
            'value' => abs( intval( $_GET['comment_count'] ) ),
            'compare' => $compare,
          );

        } elseif ( isset( $_GET['comment_count_min'] ) ) {

          $args_filter_or_custom['comment_count'] = array(
            'value' => abs( intval( $_GET['comment_count_min'] ) ),
            'compare' => '>=',
          );

        } elseif ( isset( $_GET['comment_count_max'] ) ) {

          $args_filter_or_custom['comment_count'] = array(
            'value' => abs( intval( $_GET['comment_count_max'] ) ),
            'compare' => '<=',
          );

        }


        if ( isset( $_GET['verbose_debug'] ) ) {

          $verbose_debug = true;

        } else {

          $verbose_debug = false;

        }


        /**
        * Get parameters that don't affect the retrieval of posts and that will be passed through to the final query
        */
        $query_args_pass_through_defaults = array(
          'utm_source', // Google Analytics
          'utm_campaign', // Google Analytics
          'utm_medium', // Google Analytics
          'utm_term', // Google Analytics
          'utm_content', // Google Analytics
          'pk_campaign', // Matomo Analytics
          'pk_kwd', // Matomo Analytics
          'pk_source', // Matomo Analytics
          'pk_medium', // Matomo Analytics
          'pk_content', // Matomo Analytics
        );

        foreach ( $query_args_pass_through_defaults as $value ) {

          if ( isset( $_GET[ $value ] ) ) {

            // Sanitized with sanitize_text_field because some values may be uppercase or spaces
            $query_args_pass_through[ $value ] = sanitize_text_field( $_GET[ $value ] );

          }
        }



        $php_rand = false;

        // Set up the search query depending on the criteria
        switch ( $redirect_to ) {

          // Show the latest post
          case 'latest':
          case 'last':

          $args_sorting = array(
            'orderby' => 'date',
            'order' => 'DESC',
          );

          break;

          // Show the oldest post
          case 'oldest':
          case 'first':

          $args_sorting = array(
            'orderby' => 'date',
            'order' => 'ASC',
          );

          break;

          // Show a random post
          case 'random':

          if ( $count > 0 && empty( $random_bias ) ) {

            $args_sorting = array(
              'orderby'         => 'date',
              'order'           => 'DESC',
              'posts_per_page'  => $count
            );

            $php_rand = true;

          } elseif ( $count < 0 && empty( $random_bias ) ) {

            $args_sorting = array(
              'orderby'         => 'date',
              'order'           => 'ASC',
              'posts_per_page'  => $count
            );

            $php_rand = true;

          } else {

            $args_sorting = array(
              'posts_per_page'  => -1
            );

            $php_rand = true;

          }

          break;

          // find a post based on orderby (and order)
          case 'custom':

          if ( empty( $args_filter_or_custom['orderby'] ) ) {

            _e( "Error: The parameter 'custom' requires also 'orderby'", "redirect-url-to-post" );

            exit;

          }


          // Check order parameter
          $order_whitelist = array(
            'ASC',
            'DESC'
          );

          if ( ! in_array( strtoupper( $args_filter_or_custom['order'] ), $order_whitelist ) ) {

            _e( "Error: Unrecognized value of parameter 'order'", "redirect-url-to-post" );

            exit;

          }

          // Check orderby parameter
          $orderby_whitelist = array(
            'author',
            'comment_count',
            'date',
            'ID',
            'menu_order',
            'modified',
            'name',
            'none',
            'parent',
            'rand',
            'title',
            'type',
          );

          if ( ! in_array( $args_filter_or_custom['orderby'], $orderby_whitelist ) ) {

            _e( "Error: Unrecognized value of parameter 'orderby'", "redirect-url-to-post" );

            exit;

          }


          $args_sorting = array();

          break;

          default:

          /**
          * Unrecognized value of 'redirect_to' => finish processing and let other plugins do their work
          */
          return;

          break;
        }

        if ( isset( $args_sorting ) ) {

          $args = array_merge( $args_filter_or_custom, $args_sorting );

        } else {

          $args = $args_filter_or_custom;

        }


        if ( ! empty( $random_bias ) && ! empty( $args['offset'] ) ) {

          /**
          * We'll have to apply the offset manually
          */
          $offset = $args['offset'];

          unset( $args['offset'] );

        }

        /**
        * Retrieve the post and redirect to its permalink
        */
        if ( ! empty( $caching_time ) ) {

          $key_cache = md5( serialize( $args ) . serialize( $php_rand ) );

          /**
          * We save every $key_cache as own transient so that they can have different lifetimes.
          */
          $post_ids = get_transient( 'chatty_mango_rutp_post_ids-' . $key_cache );

          if ( ! empty( $post_ids ) && defined( 'WP_DEBUG' ) && WP_DEBUG && $verbose_debug ) {

            error_log( sprintf( '[Redirect URL to Post] We found something useful in the cache for redirect_to=%s.', $redirect_to ) );

          }

        }

        if ( empty( $post_ids ) ) {

          /**
          * We have to query the database
          */

          wp_reset_postdata();

          /**
          * Remove all hooks that might cancel out our 'orderby'
          */
          remove_all_actions( 'pre_get_posts' );

          /**
          * Reduce the risk of interference from other plugins
          * (It can be turned off, for example on multilingual websites)
          */
          if ( $args_filter_or_custom['suppress_filters'] ) {

            remove_all_filters( 'posts_clauses' );
            remove_all_filters( 'posts_orderby' );
            remove_all_filters( 'posts_where' );
            remove_all_filters( 'posts_join' );
            remove_all_filters( 'posts_groupby' );

          }

          /**
          * WP_Query is supposed to sanitize $args
          */
          $the_query = new WP_Query( $args );

          $post_ids = $the_query->posts;

          if ( ! empty( $key_cache ) ) {

            /**
            * Save the result to the transient cache
            */

            if ( defined( 'WP_DEBUG' ) && WP_DEBUG && $verbose_debug ) {

              error_log( sprintf( '[Redirect URL to Post] We filled the cache for redirect_to=%s with a lifetime of %d seconds.', $redirect_to, $caching_time ) );

            }

            set_transient( 'chatty_mango_rutp_post_ids-' . $key_cache, $post_ids, $caching_time );

          }

        }

        if ( ! empty( $post_ids ) ) {

          if ( $php_rand ) {

            if ( count( $post_ids ) ) {

              if ( empty( $random_bias ) ) {

                /**
                * Pick a random post from the array
                */
                $post_id = $post_ids[ array_rand( $post_ids ) ];

              } else {

                /**
                * Get the subsection of posts that will receive the bias
                */
                if ( $count > 0 ) {

                  if ( ! empty( $offset ) ) {

                    $start = $offset;

                  } else {

                    $start = 0;

                  }

                } else {

                  if ( ! empty( $offset ) ) {

                    $start = $count - $offset; // $count is negative

                  } else {

                    $start = $count;

                  }

                }

                $bias_post_ids = array_slice( $post_ids, $start, abs( $count ) );

                if ( rand( 0, 99 ) < $random_bias ) {
                  /**
                  * Pick a post from $bias_post_ids
                  */
                  $post_id = $bias_post_ids[ array_rand( $bias_post_ids ) ];

                } else {
                  /**
                  * Pick a post from the rest
                  */
                  $post_ids_diff = array_diff( $post_ids, $bias_post_ids );

                  $post_id = $post_ids_diff[ array_rand( $post_ids_diff ) ];

                }

              }

            }

          } else {

            /**
            * Use the first number of the array
            */
            $post_id = reset( $post_ids );

          }

        }

        if ( $post_id ) {

          $permalink = get_permalink( $post_id );

          /**
          * Add query parameters that appear in the final URL
          */

          if ( ! empty( $query_args_pass_through ) ) {

            $permalink = esc_url_raw( add_query_arg( $query_args_pass_through, $permalink ) );

          }

          $this->redirect( $permalink );

        } else {

          // Nothing found, go to post with id as specified by redirect_to_default, or home

          if ( isset( $_GET['default_redirect_to'] ) ) {

            $default_redirect_to = sanitize_key( $_GET['default_redirect_to'] );

          }

          if ( empty( $default_redirect_to ) ) {

            if ( defined( 'WP_DEBUG' ) && WP_DEBUG && $verbose_debug ) {

              error_log( '[Redirect URL to Post] Nothing found, going to site URL' );

            }

            // no default given => go to home page
            $this->redirect( site_url() );

          } else {

            $permalink = get_permalink( $default_redirect_to );

            if ( $permalink === FALSE ) {

              if ( defined( 'WP_DEBUG' ) && WP_DEBUG && $verbose_debug ) {

                error_log( '[Redirect URL to Post] Nothing found and default_redirect_to not found, going to site URL.' );

              }

              // default post or page does not exist => go to home page
              $this->redirect( site_url() );

            } else {

              if ( defined( 'WP_DEBUG' ) && WP_DEBUG && $verbose_debug ) {

                error_log( '[Redirect URL to Post] Nothing found, going to default_redirect_to.' );

              }

              $this->redirect( $permalink );

            }

          }

        }

      }

    }

  }

  /**
  * Redirect to a URL
  *
  * Using own redirection so that we can add a header.
  * Firefox 57 needs to be told not to cache.
  *
  * @param string $link Permalink where to redirect to
  * @return void
  */
  private function redirect( $link ) {

    //wp_redirect( $permalink, 307 );
    /**
    * best experience with code 307 for preventing caching in browser
    * create own redirect to be able to add own header
    */
    header( 'Cache-Control: no-cache, must-revalidate' );
    header( 'Location: ' . $link, true, 307 );
    exit;

  }


  /**
  * Add Help link to plugins page
  *
  * @param array $links
  * @return array
  */
  public function add_help_link( $links ) {

    $settings_link = '<a href="https://documentation.chattymango.com/documentation/redirect-url-to-post/" target="_blank">' . __( "Help", "redirect-url-to-post" ) . '</a>';

    array_unshift( $links, $settings_link );

    return $links;

  }


  /**
  * Implements a shortcode to add a random button
  * Requires Javascript
  *
  * @param array $atts Shortcode parameters
  * @return string HTML to replace the shortcode
  */
  public function redirect_to_post_button( $atts = array() ) {

    extract( shortcode_atts( array(
      'div_class' => null,
      'button_class' => null,
      'text' => __( 'Random Post', 'redirect-url-to-post' ),
      'redirect_to' => 'random',
      'params' => null
    ), $atts ) );

    $url = get_site_url( null, '/?redirect_to=' . $redirect_to );

    /**
    * Add optional paramters
    */
    if ( ! empty( $params ) ) {

      if ( strpos( $params, '=>' ) !== false ) {

        // old format

        // split the string by commas
        $params_array = explode( ',', $params );

        foreach ( $params_array as $item ) {

          $item_array = explode( '=>', $item );

          if ( count( $item_array) == 2 ) {

            $item_array[0] = sanitize_key( trim( $item_array[0] ) );
            $item_array[1] = sanitize_title( trim( $item_array[1] ) );

            $url .= '&' . $item_array[0] . '=' . $item_array[1];

          }

        }

      } else {

        // split the string by |
        $params_array = explode( '|', $params );

        foreach ( $params_array as $item ) {

          $item_array = explode( '=', $item );

          if ( count( $item_array) == 2 ) {

            $item_array[0] = sanitize_key( trim( $item_array[0] ) );
            $item_array[1] = sanitize_title( trim( $item_array[1] ) );

            $url .= '&' . $item_array[0] . '=' . $item_array[1];

          }

        }

      }

    }

    $html = '<div';

    if ( ! empty( $div_class ) ) {

      $html .= ' class="' . sanitize_html_class( $div_class ) . '"';

    }

    $html .= '>';

    /**
    * Construct the actual button
    */
    $html .= '<button';

    if ( ! empty( $button_class ) ) {

      $html .= ' class="' . sanitize_html_class( $button_class ) . '"';

    }

    $url = str_replace( '"', '\"', $url );

    $url = str_replace( "'", '\"', $url );

    $html .= ' onclick="window.location.href=\'' . $url . '\'">' . sanitize_text_field( $text ) . '</button>';

    $html .= '</div>';

    return $html;

  }


  /**
  * Display some help on plugin activation
  *
  * @param void
  * @return void
  */
  public function on_activation() {



  }


  /**
  * Displays an admin notice
  *
  *
  * @param void
  * @return void
  */
  public function admin_notice()
  {
    /**
    * Display only for first-time activation, not after updates
    * Don't use on_activation, because then you cannot use localization
    */
    if ( get_option( 'redirect-url-to-post-onboarding', false ) === false ) {

      $plugin_link = 'https://chattymango.com/redirect-url-to-post/';

      $documentation_link = 'https://documentation.chattymango.com/documentation/redirect-url-to-post/';

      $random_post_link = get_site_url( null, '/?redirect_to=random' );
      $latest_post_link = get_site_url( null, '/?redirect_to=latest' );
      $date_query_before_post_link = get_site_url( null, '/?redirect_to=latest&before=10%20minute%20ago' );
      $date_query_after_post_link = get_site_url( null, '/?redirect_to=random&after=1%20month%20ago&cache=20' );

      $html = '<div class="notice notice-info is-dismissible"><p>' .
      '<h3>' . sprintf( __( 'Thank you for installing Redirect URL to Post!', 'redirect-url-to-post' ), 'href="' . $plugin_link . '?pk_campaign=rutp&pk_kwd=onboarding" target="_blank"' ) . '</h3>' .
      '<p>' . __( "This plugin doesn't have any settings. You configure it entirely through the URL query parameters.", 'redirect-url-to-post' ) . '</p>' .
      '<h4>' . __( 'Here are some examples:', 'redirect-url-to-post' ) . '</h4>' .
      '<ul style="list-style-type:disc; padding-left: 20px;">
      <li>' . sprintf( __( 'Go to a random post: <a %1$s>%2$s</a>', 'redirect-url-to-post' ), 'href="' . $random_post_link . '" target="_blank"', $random_post_link ) . '</li>
      <li>' . sprintf( __( 'Go to your latest post: <a %1$s>%2$s</a>', 'redirect-url-to-post' ), 'href="' . $latest_post_link . '" target="_blank"', $latest_post_link ) . '</li>
      <li>' . sprintf( __( 'Go to your latest post that was created at least 10 minutes ago: <a %1$s>%2$s</a> (We replaced all spaces by "%%20".)', 'redirect-url-to-post' ), 'href="' . $date_query_before_post_link . '" target="_blank"', $date_query_before_post_link ) . '</li>
      <li>' . sprintf( __( 'Go to a random post from the past month and use the cache: <a %1$s>%2$s</a> (or to the front page, if nothing was found)', 'redirect-url-to-post' ), 'href="' . $date_query_after_post_link . '" target="_blank"', $date_query_after_post_link ) . '</li>
      </ul>' .
      '<p>' . __( 'You can use these links anywhere in your posts and pages, in menus or in buttons. You can even send them in a newsletter. Or you could try the shortcode [redirect_to_post_button] in a page or post.', 'redirect-url-to-post' ) . '</p>' .
      '<p>' . sprintf( __( 'Please find a description of all parameters in the <a %s>plugin documentation</a>.', 'redirect-url-to-post' ), 'href="' . $documentation_link . '?pk_campaign=rutp&pk_kwd=onboarding" target="_blank"' ) . '</p>' .
      '<p>' . __( 'Have fun!', 'redirect-url-to-post' ) . '</p>' .
      '</p></div><div clear="all" /></div>';

      echo $html;

      update_option( 'redirect-url-to-post-onboarding', 1 );

    }

  }

}



/**
* launch the plugin: add actions and filters
*/
$redirectUrlToPost = new RedirectUrlToPost();


/**
* Internationalization before WP 4.6
*
* @param void
* @return void
*/
function redirect_url_to_post_load_plugin_textdomain() {

  load_plugin_textdomain( 'redirect-url-to-post', FALSE, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

}

add_action( 'init', 'redirect_url_to_post_load_plugin_textdomain' );
