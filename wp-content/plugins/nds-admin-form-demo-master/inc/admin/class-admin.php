<?php

namespace Nds_Admin_Form_Demo\Inc\Admin;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @link       https://www.nuancedesignstudio.in
 * @since      1.0.0
 *
 * @author    Karan NA Gupta
 */
class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	
	/**
	 * The text domain of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_text_domain    The text domain of this plugin.
	 */
	private $plugin_text_domain;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param    string $plugin_name	The name of this plugin.
	 * @param    string $version	The version of this plugin.
	 * @param	 string $plugin_text_domain	The text domain of this plugin
	 */
	public function __construct( $plugin_name, $version, $plugin_text_domain ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->plugin_text_domain = $plugin_text_domain;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/nds-admin-form-demo-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		
		$params = array ( 'ajaxurl' => admin_url( 'admin-ajax.php' ) );
		wp_enqueue_script( 'nds_ajax_handle', plugin_dir_url( __FILE__ ) . 'js/nds-admin-form-demo-ajax-handler.js', array( 'jquery' ), $this->version, false );				
		wp_localize_script( 'nds_ajax_handle', 'params', $params );		

	}
	
	/**
	 * Callback for the admin menu
	 * 
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {
				
		add_menu_page(	__( 'Admin Form Demo', $this->plugin_text_domain ), //page title
						__( 'Admin Form Demo', $this->plugin_text_domain ), //menu title
						'manage_options', //capability
						$this->plugin_name //menu_slug
					);
		
		 // Add a submenu page and save the returned hook suffix.
		$html_form_page_hook = add_submenu_page( 
									$this->plugin_name, //parent slug
									__( 'Admin Form Demo', $this->plugin_text_domain ), //page title
									__( 'HTML Form Submit', $this->plugin_text_domain ), //menu title
									'manage_options', //capability
									$this->plugin_name, //menu_slug
									array( $this, 'html_form_page_content' ) //callback for page content
									);
		
		// Add a submenu page and save the returned hook suffix.
//		$ajax_form_page_hook = add_submenu_page(
//									$this->plugin_name, //parent slug
//									__( 'Admin Form Demo', $this->plugin_text_domain ), //page title
//									__( 'Ajax Form Sumit', $this->plugin_text_domain ), //menu title
//									'manage_options', //capability
//									$this->plugin_name . '-ajax', //menu_slug
//									array( $this, 'ajax_form_page_content' ) //callback for page content
//									);
		
		/*
		 * The $page_hook_suffix can be combined with the load-($page_hook) action hook
		 * https://codex.wordpress.org/Plugin_API/Action_Reference/load-(page) 
		 * 
		 * The callback below will be called when the respective page is loaded
		 */				
		add_action( 'load-'.$html_form_page_hook, array( $this, 'loaded_html_form_submenu_page' ) );
//		add_action( 'load-'.$ajax_form_page_hook, array( $this, 'loaded_ajax_form_submenu_page' ) );
	}
	
	/*
	 * Callback for the add_submenu_page action hook
	 * 
	 * The plugin's HTML form is loaded from here
	 * 
	 * @since	1.0.0
	 */
	public function html_form_page_content() {
		//show the form
		include_once( 'views/partials-html-form-view.php' );
	}
	
	/*
	 * Callback for the add_submenu_page action hook
	 * 
	 * The plugin's HTML Ajax is loaded from here
	 * 
	 * @since	1.0.0
	 */
	public function ajax_form_page_content() {
		include_once( 'views/partials-ajax-form-view.php' );
	}	
	
	/*
	 * Callback for the load-($html_form_page_hook)	 
	 * Called when the plugin's submenu HTML form page is loaded
	 * 
	 * @since	1.0.0
	 */
	public function loaded_html_form_submenu_page() {
		// called when the particular page is loaded.
	}
	
	/*
	 * Callback for the load-($ajax_form_page_hook)	 
	 * Called when the plugin's submenu Ajax form page is loaded
	 * 
	 * @since	1.0.0
	 */
	public function loaded_ajax_form_submenu_page() {
		// called when the particular page is loaded.		
	}
	
	/**
	 * 
	 * @since    1.0.0
	 */
	public function the_form_response() {
		
		if( isset( $_POST['nds_add_user_meta_nonce'] ) && wp_verify_nonce( $_POST['nds_add_user_meta_nonce'], 'nds_add_user_meta_form_nonce') ) {
//			$nds_user_meta_key = sanitize_key( $_POST['nds']['user_meta_key'] );
//			$nds_user_meta_value = sanitize_text_field( $_POST['nds']['user_meta_value'] );
//			$nds_user =  get_user_by( 'login',  $_POST['nds']['user_select'] );
//			$nds_user_id = absint( $nds_user->ID ) ;
			
			// server processing logic
			
			if( isset( $_POST['ajaxrequest'] ) && $_POST['ajaxrequest'] === 'true' ) {
				// server response
				echo '<pre>';					
					print_r( $_POST );
				echo '</pre>';				
				wp_die();
            }

            //Function call to Parse CSV and Store Data to WP_POSTS
			if($this->parseCSV($_POST)){
				// server response
				$admin_notice = "Success";
				$this->custom_redirect( $admin_notice, $_POST );
				exit;
			}
			// server response
			$admin_notice = "Failed";
			$this->custom_redirect( $admin_notice, $_POST );
			exit;
		}
		else {
			wp_die( __( 'Invalid nonce specified', $this->plugin_name ), __( 'Error', $this->plugin_name ), array(
						'response' 	=> 403,
						'back_link' => 'admin.php?page=' . $this->plugin_name,

				) );
		}
	}

	/**
	 * Redirect
	 * 
	 * @since    1.0.0
	 */
	public function custom_redirect( $admin_notice, $response ) {
		wp_redirect( esc_url_raw( add_query_arg( array(
									'nds_admin_add_notice' => $admin_notice,
									'nds_response' => $response,
									),
							admin_url('admin.php?page='. $this->plugin_name ) 
					) ) );

	}


	/**
	 * Print Admin Notices
	 * 
	 * @since    1.0.0
	 */
	public function print_plugin_admin_notices() {              
		  if ( isset( $_REQUEST['nds_admin_add_notice'] ) ) {
			if( $_REQUEST['nds_admin_add_notice'] === "success") {
				$html =	'<div class="notice notice-success is-dismissible"> 
							<p><strong>The request was successful. </strong></p><br>';
				$html .= '<pre>' . htmlspecialchars( print_r( $_REQUEST['nds_response'], true) ) . '</pre></div>';
				echo $html;
			}
			
			// handle other types of form notices

		  }
		  else {
			  return;
		  }
	}


	protected function parseCSV(){
		$result = FALSE;
		$tmpName = $_FILES['csv']['tmp_name'];
		//$csvAsArray = array_map('str_getcsv', file($tmpName));
		/* Map Rows and Loop Through Them */
		$rows   = array_map('str_getcsv', file($tmpName));
		$header = array_shift($rows);
		$csvAsArray    = array();
		foreach($rows as $row) {
			$csvAsArray[] = array_combine($header, $row);
		}

		//echo "<pre>";print_r($csvAsArray);die();
		$args = array(
			'hide_empty'      => false,
		);
		$categories = get_categories($args);
		$catArray = array();
		if(!empty($categories)){
			foreach ($categories as $category){
				$catArray[$category->term_id] = $category->slug;
			}
		}
		//echo "<pre>";print_r($csvAsArray);die();
		foreach ($csvAsArray as $row){

			if(!empty($row['HEADLINE']) && !empty($row['QUOTE']) && !empty($row['IMAGE URL'])){
//				$data['postTitle'] = $row['HEADLINE'];
//				$data['postContent'] = $row['QUOTE'];
				$postId = $this->insertPost(array('postTitle' => $row['HEADLINE'],'postContent' => $row['QUOTE']));
				if($postId > 0){
					$result = TRUE;
					//$this->setFeaturedImage($postId,$row['IMAGE URL']);
					$this->setFeaturedImageFromUploads($postId,$row['IMAGE URL']);
					$this->setPostMeta($postId,$row);
					//Extract Tags and add to tje post
					$tags = $this->extractTags($row);
					if(!empty($tags)){
						$tags = implode(',',$tags);
						wp_set_post_tags($postId,$tags,true);
					}

					if(isset($row['CATEGORY']) && !empty($row['CATEGORY'])){
						$catName = $row['CATEGORY'];
						$catDescription = $row['CATEGORY'];
						$catNickName = trim(strtolower($row['CATEGORY']));
						$categoryId = array_search($catNickName, $catArray);
						if(!$categoryId){
							$categoryId = wp_insert_category(
								array(
									'cat_name' 				=> $catName,
									'category_description'	=> $catDescription,
									'category_nicename' 		=> $catNickName,
									'taxonomy' 				=> 'category'
								)
							);
						}
						wp_set_post_categories($postId,array($categoryId));
					}
				}
			}
		}
		return $result;
	}

	protected function extractTags($data = array()){
		$tags = array();
		foreach ($data as $key => $value) {
			if (strpos($key, 'TAG') === 0) {
				if(!empty($value))
				$tags[] = $value;
			}
		}
		return $tags;
	}

	protected function insertPost($data = array()){
		$post = array();
		$post['post_status']   = 'publish';
		$post['post_type']     = 'post'; // can be a Custom Post Type too
		$post['post_title']    = $data['postTitle'];
		$post['post_content']  = $data['postContent'];
		//$post['post_author']   = 1;//can change to dynamic value later

		// Create Post
		$postId = wp_insert_post( $post );
//		echo $postId."<br/>";
		return $postId;
	}

	protected function setFeaturedImageFromUploads($parentPostId,$imageUrl){
		// Add Featured Image to Post
		if(empty($imageUrl))
			$imageUrl = 'https://stufftrumpsays.com/wp-content/uploads/2019/08/stuff-trump-says-default-header-web-1.jpg'; // Default Image

		// Check the type of file. We'll use this as the 'post_mime_type'.
		$filetype = wp_check_filetype( basename( $imageUrl ), null );

		// Get the path to the upload directory.
		$wp_upload_dir = wp_upload_dir();

		if(@getimagesize($wp_upload_dir['url'] . '/' . basename( $imageUrl ))){
			$imageUrl = $wp_upload_dir['url'] . '/' . basename( $imageUrl );
		}

		// Prepare an array of post data for the attachment.
		$attachment = array(
			'guid'           => $imageUrl,
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $imageUrl ) ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);

		// Insert the attachment.
		$attach_id = wp_insert_attachment( $attachment, $imageUrl, $parentPostId );

		// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Generate the metadata for the attachment, and update the database record.
		$attach_data = wp_generate_attachment_metadata( $attach_id, $imageUrl );
		wp_update_attachment_metadata( $attach_id, $attach_data );

		set_post_thumbnail( $parentPostId, $attach_id );
	}

	protected function setFeaturedImage($postId,$imageUrl = '',$imageName = ''){
		// Add Featured Image to Post
		$image_url        = 'https://stufftrumpsays.com/wp-content/uploads/2019/08/stuff-trump-says-default-header-web-1.jpg'; // Default Image
		//$image_name       = 'stuff-trump-says-'.rand().'-default.jpg';
		if(!empty($imageUrl))
			$image_url = $imageUrl;
		if(!empty($imageName))
			$image_name = $imageName;
		$upload_dir       = wp_upload_dir(); // Set upload folder
		$image_data       = file_get_contents($image_url); // Get image data
		//$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
		$filename         = basename( $image_url ); // Create image file name

		// Check folder permission and define file location
		if( wp_mkdir_p( $upload_dir['path'] ) ) {
			$file = $upload_dir['path'] . '/' . $filename;
		} else {
			$file = $upload_dir['basedir'] . '/' . $filename;
		}

		// Create the image  file on the server
		file_put_contents( $file, $image_data );

		// Check image file type
		$wpFileType = wp_check_filetype( $filename, null );

		// Set attachment data
		$attachment = array(
			'post_mime_type' => $wpFileType['type'],
			'post_title'     => sanitize_file_name( $filename ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);

		// Create the attachment
		$attachId = wp_insert_attachment( $attachment, $file, $postId );

		// Include image.php
		require_once(ABSPATH . 'wp-admin/includes/image.php');

		// Define attachment metadata
		$attachData = wp_generate_attachment_metadata( $attachId, $file );

		// Assign metadata to attachment
		wp_update_attachment_metadata( $attachId, $attachData );

		// And finally assign featured image to post
		set_post_thumbnail( $postId, $attachId );
	}

	protected function setPostMeta($postId,$metaData = array()){
		//Update inserts a new entry if it doesn't exist, updates otherwise
		update_post_meta($postId, 'stspostdata', $metaData);

		//To Retrive for usage
		//$metaData = get_post_meta($postId, 'stspostdata');

	}
}