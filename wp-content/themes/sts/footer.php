<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sts
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="footer-follow">
		<p>Get Stuff Trump Says on Social</p>
			<div class="follow-icons">
				<a href="https://twitter.com/StuffDTSays" target="_blank">
				<img src="https://stufftrumpsays.com/wp-content/uploads/2019/09/sts_social_follow_twitter.png" alt="stuff-trump-says-follow-twitter">
				</a>
				<a href="https://www.facebook.com/GetStuffTrumpSays" target="_blank">
				<img src="https://stufftrumpsays.com/wp-content/uploads/2019/09/sts_social_follow_facebook.png" alt="stuff-trump-says-follow-fb">
				</a>
				<a href="https://www.instagram.com/getstufftrumpsays" target="_blank">
				<img src="https://stufftrumpsays.com/wp-content/uploads/2019/09/sts_social_follow_instagram.png" alt="stuff-trump-says-follow-insta">
				</a>
				
			</div>
		</div>
		<nav id="footer-navigation" class="bottom-navigation">
			<div class="menu-footer-container">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-2',
				'menu_id'        => 'footer-menu',
			) );
			?>
			</div>
		</nav><!-- #footer-navigation -->
		<div class="site-info">
			&copy;2019
			<span class="sep"> | </span>
			Stuff Trump Says
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
