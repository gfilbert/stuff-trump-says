<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sts
 */

get_header();

function removeEmptyValues(array &$array){
	foreach ($array as $key => &$value) {
		if (is_array($value)) {
			$value = removeEmptyValues($value);
		}
		if (empty($value)) {
			unset($array[$key]);
		}
	}
	return $array;
}

$postmeta = get_post_meta(get_the_ID(),'stspostdata');
$postmeta = removeEmptyValues($postmeta);

// Refrence Page : https://crunchify.com/how-to-create-social-sharing-button-without-any-plugin-and-script-loading-wordpress-speed-optimization-goal
// Get current page URL
$socialMediaURL = urlencode(get_permalink());
// Get current page title
$socialMediaTitle = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');

// Construct sharing URL without using any script
$twitterURL = 'https://twitter.com/intent/tweet?text='.$socialMediaTitle.'&amp;url='.$socialMediaURL;
$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$socialMediaURL;


$googleURL = 'https://plus.google.com/share?url='.$socialMediaURL;
$bufferURL = 'https://bufferapp.com/add?url='.$socialMediaURL.'&amp;text='.$socialMediaTitle;
$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$socialMediaURL.'&amp;title='.$socialMediaTitle;

$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$socialMediaURL.'&amp;media='.$crunchifyThumbnail[0].'&amp;description='.$socialMediaTitle;
?>

	
		
		<div id="leaderboard" class="above-content-widget" style="display:block;width:100%;">
		<?php if ( is_active_sidebar( 'widget-above-content' ) ) : ?>
			<?php dynamic_sidebar( 'widget-above-content' ); ?>
		<?php endif; ?>
		</div>
	<div id="primary" class="content-area">
		<div id="left-rail" class="left-rail-widgets">
			<?php if ( is_active_sidebar( 'left-sidebar' ) ) : ?>
				<?php dynamic_sidebar( 'left-sidebar' ); ?>
			<?php endif; ?>
		</div>
		<main id="main" class="site-main">
            <article id="post-48" class="post-48 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized">
                <header class="entry-header">
                    <h1 class="entry-title">Stuff Trump Says 1</h1>
                </header><!-- .entry-header -->
				<div class="sts-featured-img">
                	<?php the_post_thumbnail() ?>
				</div>
                <!-- .post-thumbnail -->
                <div class="entry-content">
                    <?php if(isset($postmeta[0]['IMAGE SOURCE'])){ ?>
                        <p class="image-source">Image Source: <?php echo $postmeta[0]['IMAGE SOURCE'] ?></p>
                    <?php } ?>
                    <div class="wp-block-columns has-2-columns quote-columns">
                        <div class="wp-block-column">
                            <figure class="wp-block-image fb"><a href="<?php echo $facebookURL ?>" target="_blank" rel="noreferrer noopener"><img src="https://stufftrumpsays.com/wp-content/uploads/2019/09/social_icons_facebook.png" alt="" class="wp-image-57"></a></figure>
                            <figure class="wp-block-image twitter"><a href="<?php echo $twitterURL ?>" target="_blank" rel="noreferrer noopener"><img src="https://stufftrumpsays.com/wp-content/uploads/2019/09/social_icons_twitter.png" alt="" class="wp-image-58"></a></figure>
							<figure class="wp-block-image mail">
                            <a rel="noreferrer noopener" href="mailto:?subject=I%20wanted%20to%20share%20this%20post%20with%20you%20from <?php bloginfo('name'); ?>&body=<?php the_title('','',true); ?>%20%20%3A%20%20<?php echo get_the_excerpt(); ?>%20%20%2D%20%20%28%20<?php the_permalink(); ?>%20%29" title="Email to a friend/colleague" target="_blank">
                            <img src="https://stufftrumpsays.com/wp-content/uploads/2019/09/social_icons_email.png" alt="" class="wp-image-59">
                            </a>
                            </figure>
<!--                            <figure class="wp-block-image mail"><a href="#" target="_blank" rel="noreferrer noopener"><img src="https://stufftrumpsays.com/wp-content/uploads/2019/09/social_icons_email.png" alt="" class="wp-image-59"></a></figure>-->
                            <?php if(isset($postmeta[0]['FLIP FLOP BUTTON']) && isset($postmeta[0]['FLIP FLOP LINK'])){
	                            $flipPost = get_page_by_title($postmeta[0]['FLIP FLOP LINK'], OBJECT, 'post');
                            ?>
                                <div class="wp-block-button aligncenter flip-flop"><a class="wp-block-button__link" href="<?php echo $flipPost->guid ?>">Flip-Flop</a></div>
                            <?php } ?>
	                		<?php if (function_exists('dw_reactions')) { dw_reactions(); } ?>

                        </div>
                        <div class="wp-block-column">
                            <p class="quote">
                                <?php
                                    while ( have_posts() ) :
                                        the_post();
	                                    $content = get_the_content();
	                                     echo stripslashes(wp_filter_nohtml_kses( $content ));
                                    endwhile; // End of the loop.
                                ?>
                            </p>
                            <?php if(isset($postmeta[0]['SOURCE'])){ ?>
                                <p class="source-context"><span class="quote-source"><?php echo $postmeta[0]['SOURCE'] ?></span>
	                             | <span class="quote-context"><?php echo $postmeta[0]['CONTEXT'] ?></span>
                                </p>
                            <?php } ?>
                        </div>
                    </div>
                    <?php
                        $permaLink = get_the_permalink();
                        //Create WordPress Query with 'orderby' set to 'rand' (Random)
                        $the_query = new WP_Query( array ( 'orderby' => 'rand', 'posts_per_page' => '1', 'exclude' => get_the_ID() ) );
                        // output the random post
                        while ( $the_query->have_posts() ) : $the_query->the_post();
                            $permaLink = get_the_permalink();
                        endwhile;

                        // Reset Post Data
                        wp_reset_postdata();
						$buttonText = "Read the next one! I'm AMAZING!";
                       $buttonText = getRandomButtonText();
                    ?>
                    <div class="wp-block-button generate-quote"><a href=<?php echo $permaLink; ?> class="wp-block-button__link"><?php echo $buttonText ?></a></div>
					<div class="utm-ad-change">
						<?php
						if(isset($_GET['utm_source'])){
						switch (strtolower($_GET['utm_source'])) {
						case 'cad':
							adsforwp_the_ad( 963 );
						break;
						case 'taboola':
							adsforwp_the_ad( 965 );
						break;
						case 'contentad_push':
							adsforwp_the_ad( 963 );
						break;
						case 'outbrain':
							?>
							<div class="OUTBRAIN" data-src="https://stufftrumpsays.com" data-widget-id="TF_6"></div> <script type="text/javascript" async="async" src="//widgets.outbrain.com/outbrain.js"></script>
							<?php
						break;
						case 'fb_ads':

						break;
						case 'google':

						break;
						default:
							adsforwp_the_ad( 963 );
						}
						}else{
							adsforwp_the_ad( 963 );
						}
						?>
					</div>
					<?php if ( is_active_sidebar( 'widget-below-content' ) ) : ?>
						<?php dynamic_sidebar( 'widget-below-content' ); ?>
					<?php endif; ?>
                </div><!-- .entry-content -->

                <footer class="entry-footer">
                    <span class="cat-links">Posted in <a href="https://stufftrumpsays.com/category/uncategorized/" rel="category tag">Uncategorized</a></span>
                </footer><!-- .entry-footer -->
            </article>
		</main><!-- #main -->
		<div id="right-rail" class="right-rail-widgets">
			<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>
				<?php dynamic_sidebar( 'right-sidebar' ); ?>
			<?php endif; ?>
		</div>
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();